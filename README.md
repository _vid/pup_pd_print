# README #
## PUP Position Description Print ##

[Jump to Installation Instructions](#markdown-header-to-install)

### Purpose ###

Create a printable version of the Position Description, leveraging the existing PageUp People (PUP) print style sheet, adding an some additional UO styles and resetting some fixed widths that are too wide to print.

### What is it ###

Files: 
 * pup_pd_print.js
    * A JavaScript file that sets all media: unspecified stylesheets to 'screen', updates divs with fixed width to width: 90%; max-width; [previous value]px.
	    * Also appends 3 stylesheets with CSS Reset for printing, UO styles and UO Printable styles. Primarily hideing UI elements during printing, applying some stying to labels and a fix for textareas and chrome accomodations

### How it works ###

Note: This is a proof of concept;

When the JavaScript (pup_pd_print.js) is called, the stylesheet media states are set and an additional style reset and printable stylesheet is added.
Lastly print dialog box will open on run.

The JavaScript source code can be run locally or called from a remote host. No remote resources are called. This script only interacts w/ elements available on the current page.

It's assumed that initially this script will be called upon clicking a link embedded in a markup field on the page.

Note: for testing you can set testingMode=true; prior to running the script to visually see the printable page.

### Installation Notes ###

You will need to be able to run the JavaScript on the position description page.
There are several options to do this. For example you could open a console in your browser and paste in the code from: pup_pd_print.js.

Alternatively you could store the JavaScript in a [bookmarklet (wikipedia ref.)](https://en.wikipedia.org/wiki/Bookmarklet).

There are two types of bookmarklets to choose from.
The bookmarklet can refer to the remote source code, or host it locally. 
There are advantages to both methods; In short, use the remote source code version if you trust the source and want to use the latest version as it's available. Think: cloud hosted.

Use the locally hosted version if you want control over the source code and you don't anticipate any changes.

The master branch of this repository is geared toward remote hosted source code.

The stand-alone alone branch accommodates a locally hosted bookmarklet but may have few features due to single-line storage limitations.

#### To Install ####
To install this bookmarklet, drag the following link to your bookmark toolbar and replace the source with the source code below.
[PUP PD Print](replace me)

The source code of the remote hosted bookmarklet is:
`javascript:
void(z=document.body.appendChild(document.createElement('script')));
void(z.language='javascript');
void(z.type='text/javascript');
void(z.id='pup_pd_print');
void(z.src='https://hr.uoregon.edu/pup_pd_print');
`

**Note: Currently in practice: **
As a link embedded in the page:
`<a href="#" onclick="javascript:
void(z=document.body.appendChild(document.createElement('script')));
void(z.language='javascript');
void(z.type='text/javascript');
void(z.src='https://hr.uoregon.edu/pup_pd_print');
void(z.id='pup_pd_print');
return false;">Print</a>
`

Or as one line:
`<a href="#" onclick="javascript: void(z=document.body.appendChild(document.createElement('script'))); void(z.language='javascript'); void(z.type='text/javascript'); void(z.src='https://hr.uoregon.edu/pup_pd_print'); void(z.id='pup_pd_print'); return false;">Print</a>`

Side note: if you would rather link to a specific version of the code you can replace the branch name (master) with the version (tag name) in the src URL.
Ex. for tag pup_pd_print-1.2:
src='https://bitbucket.org/_vid/pup_pd_print/raw/pup_pd_print-1.2/pup_pd_print.js'; Note that js served from bitbucket is mime type 'text/plain' and wont run in Chrome.

### Usage ###

 * Log in and Authenticate to your PUP account: https://admin.pageuppeople.com
 * Navigate to manage requisitions: https://admin.dc4.pageuppeople.com/PositionDescription
 * Open a position description
 * Click the bookmarklet / or the embedded print link
 * Print
 * Enjoy the printable-ness

### Road Map ###

### Current maintainer ###

* Vid Rowan ([_vid](http://drupal.org/user/631512/))