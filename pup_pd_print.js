//PUP PD Print
/**
 * Notes:
 * ex url:
 * https://admin.dc4.pageuppeople.com/v5.3/provider/manageJobs/editJob.asp?sData=3RVUME3SWSW2eKsiEAJMvcns2oIMmZ3ybh8P5hXyfnOhghoYPvUhUCTHV9cZ4mwYdXa9Gmm_TMdJlU2YqgJflOwbT43JsBbj_ivZZJsE82bLoOegSVEL-zffREqzhmJHQtRgSMOOOa8r44O96ieNdNwfxwOYmNeGDs44Ym8FUOJxh9_dGcYXAIIyCBfcllZ4Kl8NOinQ8wWLl2vYPw-ApA~~
 *
 * //Print style sheet
 * ../_css/print.css?v=5a528589dbe882514d4f5133da6cea97364f0135
 *
 * They already have a "print" menu option from the "info" menu.
 * The page opens in a new window and URL contains:
 * &bPrintDisplay=true
 *
 * But it still looks the same.
 *
 * Note: body has class: .white
 *
 *
 * Potential suggestions:
 * * Set the other two stylesheets on the page to be media ="screen".
 *     That way when printing, you would only get the single stylesheet and not the other two which seem to be overwriting all of the printable styles.
 * * There are two major divs in the document with a fixed width (in a style tag of the div) of 800 pixels.
 *   Recommendation: max-width: 800px; width: 90%; min-width: 200px;  text-align: left; margin: auto
 *     May have to use JavaScript to override that
 */
if ( typeof(testingMode) == "undefined" ) {
  testingMode = false;
}
//Code tests:
//##############################################################################################################################

//update existing non-print stylesheets to be screen only
//ref: http://stackoverflow.com/questions/1679577/get-document-stylesheets-by-name-instead-of-index

//Processing
//Step one, set all general stylesheet to "screen"
for (var i in document.getElementsByTagName("link")) {
  //console.log("#" + i);
  if(!isNaN(parseInt(i))){ //Chrome fix, only affect obj's once. Chrome affects the objects twice (once for enumerated obj. and once for any of those same obj's w/ an ID)
    thisLink = document.getElementsByTagName("link") [i];
    //console.log(thisLink);
    if (thisLink.type == "text/css" || thisLink.rel == "stylesheet") {
      //console.log("#" + i);
      //console.log(thisLink.media);
      //console.log(typeof (thisLink.media));
      if(thisLink.media !== "print" && thisLink.media !== "screen"){
        thisLink.media = "screen";
      }
    }
  }
}

/** fix textarea height
 * 1: hide the textarea during printing
 * 2: clone the textarea content into a new div (not a textarea) this is visible during printing
 * * <div class="textarea-print-helper"></div>
 * 3: Set styles for printable textarea->div
    .textarea-print-helper {
      display: block;
      overflow: visible;
      white-space: pre;
      white-space: pre-wrap;
    }
    textarea {
      display: none;
    }
 * Resource: http://stackoverflow.com/questions/4435906/print-when-textarea-has-overflow
 */
var freshDiv, textareaNode;
for (var i in document.getElementsByTagName("textarea")) {
  if(!isNaN(parseInt(i))){ //Chrome fix, only affect obj's once. Chrome affects the objects twice (once for enumerated obj. and once for any of those same obj's w/ an ID)
    thisTextarea = document.getElementsByTagName("textarea")[i];
    //create new div
    if ( ! document.getElementById("textarea-print-helper-" + i) ){
      freshDiv = document.createElement("div");                    // Create a <div> node
      freshDiv.className += "textarea-print-helper";
      freshDiv.id = "textarea-print-helper-" + i;
      textareaNode = document.createTextNode(thisTextarea.value);  // Create a text node
      freshDiv.appendChild(textareaNode);                          // Append the text to <div>
      if(thisTextarea.parentNode){
        thisTextarea.parentNode.appendChild(freshDiv);             // Append <div> to textarea parent
        thisTextarea.parentNode.parentNode.className += "textarea-tr";
      }
    } else {
      freshDiv = document.getElementById("textarea-print-helper-" + i);
      freshDiv.innerHTML = thisTextarea.value;  // Create a text node
    }
  }
  freshDiv=null;
  textareaNode=null;
}


//Find style tags w/ hard-coded 800px width and fix it.
for (var i in document.getElementsByTagName("div")) {
  if(!isNaN(parseInt(i))){ //Chrome fix, only affect obj's once. Chrome affects the objects twice (once for enumerated obj. and once for any of those same obj's w/ an ID)
    thisDiv = document.getElementsByTagName("div") [i];
    if (thisDiv.style) {
      if (thisDiv.style.width && thisDiv.style.width.search("px") > -1){
        //console.log(thisDiv.style.width);
        //console.log("yup");
        thisDiv.style.maxWidth = thisDiv.style.width;
        thisDiv.style.width = "90%";
        thisDiv.style.minWidth = "200px";
      }
    }
  }
}

//add stylesheet
/** Styles **/
var pupHead = document.head || document.getElementsByTagName("head")[0];
if ( ! document.getElementById("pup-pd-style-reset") ){
  //Prepend css reset
  var cssReset = " /* http://meyerweb.com/eric/tools/css/reset/     v2.0 | 20110126    License: none (public domain) */  html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed,  figure, figcaption, footer, header, hgroup,  menu, nav, output, ruby, section, summary, time, mark, audio, video {   margin: 0;   padding: 0;   border: 0;   font-size: 100%;   font: inherit;   vertical-align: baseline; } /* HTML5 display-role reset for older browsers */ article, aside, details, figcaption, figure,  footer, header, hgroup, menu, nav, section {   display: block; } body {   line-height: 1; } ol, ul {   list-style: none; } blockquote, q {   quotes: none; } blockquote:before, blockquote:after, q:before, q:after {   content: '';   content: none; } table {   border-collapse: collapse;   border-spacing: 0; } body {font-size: 10pt;}";

  var pupCssReset = document.createElement("style");

  //Style element
  pupCssReset.type = "text/css";
  pupCssReset.media = "print";
  pupCssReset.id = "pup-pd-style-reset";
  if (pupCssReset.styleSheet){
    pupCssReset.styleSheet.cssText = cssReset;
  } else {
    pupCssReset.appendChild(document.createTextNode(cssReset));
  }
  pupHead.insertBefore( pupCssReset, pupHead.firstChild );
}//end if not pup-pd-style-reset

if ( ! document.getElementById("pup-pd-uo-styles") ){
  //Add UO specific page styles
  var pupCss = ".textarea-print-helper {display:none; visibility:hidden;}";
  pupStyle = document.createElement("style");

  //Style element
  pupStyle.type = "text/css";
  pupStyle.media = "screen"; //aural, braille, embossed, handheld, projection, screen, tty, tv";
  pupStyle.id = "pup-pd-uo-styles";
  if (pupStyle.styleSheet){
    pupStyle.styleSheet.cssText = pupCss;
  } else {
    pupStyle.appendChild(document.createTextNode(pupCss));
  }
  pupHead.appendChild(pupStyle);
}//end if not document.getElementById("pup-pd-uo-styles") ){


if ( ! document.getElementById("pup-pd-uo-styles-print") ){
  //Add UO specific print styles after
  /* hide interface elements from print */
  var pupCss = "", pupCssArr = Array("#liveNotificationInfo, .icon_revHistory, .jobInfoHolder, .tab_active_wrapper, .tab_active, .tab_inactive_wrapper, .tab_inactive, .tabs span.tab_active_wrapper, .tabs span.tab_inactive_wrapper, #tabItem_tab_editJob_0 tr:first-child, .button, .cancelBtn, .tableAction, h1.title { display:none; visibility:hidden; margin:0;}");
  pupCssArr.push("h1#jobTitle {font-weight: bold; font-size:1.5em; line-height: 1em;}");
  pupCssArr.push("h2 {font-size: 1em;}");
  pupCssArr.push("#pge_editJob .frame, .tabs, p {margin:0; padding:0;}");
  /*hide border on page*/
  pupCssArr.push(".tabs .frame {border-width:0;}");
  /*hide border on page*/
  pupCssArr.push(".tabs .frame {border-width:0;}");
  pupCssArr.push(".chosen-single span, .fieldCaption { color: #4e4e4e; font-style: italic; font-size:small; white-space: nowrap; } ");
  pupCssArr.push(".chosen-drop input {min-width:100px;}");
  pupCssArr.push(".fieldCaption {white-space: normal;}");
  pupCssArr.push(".textarea-print-helper { display: block; visibility:visible; overflow: visible; white-space: pre; white-space: pre-wrap; padding: 5px 0; margin: 5px 0; }");
  pupCssArr.push(".textarea-tr td { border: 1px dotted grey; border-width: 1px 1px 1px 0px; }");
  pupCssArr.push(".textarea-tr td.editJobLabel { border-width: 1px 0 1px 1px; margin-left: 5px; }");
  pupCssArr.push("textarea {display: none;}");

  pupCss = pupCssArr.join("\n");
  pupPrintStyle = document.createElement("style");

  //Style element
  pupPrintStyle.type = "text/css";
  pupPrintStyle.media = "print";
  pupPrintStyle.id = "pup-pd-uo-styles-print";
  if (pupPrintStyle.styleSheet){
    pupPrintStyle.styleSheet.cssText = pupCss;
  } else {
    pupPrintStyle.appendChild(document.createTextNode(pupCss));
  }
  pupHead.appendChild(pupPrintStyle);
}//end if not pup-pd-uo-styles-print


//For testing: flip the media types (e.g. show the print view):
if (testingMode){
  for (var i in document.getElementsByTagName("link")) {
    if(!isNaN(parseInt(i))){ //Chrome fix, only affect obj's once. Chrome affects the objects twice (once for enumerated obj. and once for any of those same obj's w/ an ID)
      //console.log("#" + i);
      thisLink = document.getElementsByTagName("link") [i];
      //console.log(thisLink);
      if (thisLink.type == "text/css" || thisLink.rel == "stylesheet") {
        //console.log("#" + i);
        //console.log(thisLink.media);
        //console.log(typeof (thisLink.media));
        if(thisLink.media == "print"){
          thisLink.media = "screen";
        }else{
          thisLink.media = "print";
        }
      }
    }
  }
  for (var i in document.getElementsByTagName("style")) {
    //console.log("#" + i);
    if(!isNaN(parseInt(i))){ //Chrome fix, only affect obj's once. Chrome affects the objects twice (once for enumerated obj. and once for any of those same obj's w/ an ID)
      thisStyle = document.getElementsByTagName("style") [i];
      if (thisStyle.type == "text/css") {
        //console.log("before: "); console.log(thisStyle);
        //console.log("#" + i);
        //console.log(thisStyle.media);
        //console.log(typeof (thisStyle.media));
        if(thisStyle.media == "print"){
          //console.log("was print, changing to screen");
          thisStyle.media = "screen";
          //console.log("Changed media type to: " + thisStyle.media);
        }else{
          //console.log("was screen, changing to print");
          thisStyle.media = "print";
          //console.log("Changed media type to: " + thisStyle.media);
        }
        //console.log("after: "); console.log(thisStyle);
      }
    }
  }
}

//print dialog
window.print();
